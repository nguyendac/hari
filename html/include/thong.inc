<section class="st_video">
  <div class="row">
    <div class="ttl_video">
      <h2>「美容鍼灸を学びたい方」<br class="show_sp">「顧客満足度をあげたい！」<br>そんな方には、</h2>
      <div class="bx_ttl_sub">
        <div class="bx_left"><p>動画で<br>予習・復習<br>セミナーで学ぶ</p></div><!--/.bx_left-->
        <div class="bx_right"><p>美容鍼灸修了証発行セミナー</p><p>がおすすめ！！</p></div>
      </div>
      <!--/.bx_ttl_sub-->
    </div>
    <!--/.ttl_video-->
    <div class="move_youtube"><iframe width="640" height="360" src="https://www.youtube.com/embed/ifSHM7B6K5w?rel=0" frameborder="0" allowfullscreen></iframe></div>
  </div>
</section>
<!--/.video-->
<section class="st_introduction">
  <div class="row bkg_common">
    <div class="bx_bkg">
      <div class="inner">
        <div class="gr_intro_beauty">
          <div class="bx_ttl_intro">
            <h2 class="ttl_intro">美容鍼灸修了証発行セミナーのご紹介</h2>
          </div>
          <!--/.bx_ttl_intro-->
          <div class="dac_banner">
            <img src="images/banner.png" alt="banner">
          </div>
          <!--/.bx_top_animation-->
          <div class="txt_center">
            <p>「<span>世界美容大会日本代<br class="show_sp">表の美容鍼灸技術</span>」を<strong>動画</strong>で<strong>修得</strong>。<br>セミナーでより細部の技術と知識を習得！</p>
          </div>
          <!--/.txt_center-->
          <div class="bx_gr_intro">
            <h3 class="ttl_gr"><span>&minus;</span>セミナー項目採点基準<span>&minus;</span></h3>
            <ul class="list_intro">
              <li>
                <span>1.オイル・塗布</span>
                <img src="../images/3/intro_01.jpg" alt="Intro 01">
                <div class="txt">オイルでお肌全体の保湿、ハリ、艶を高め、お顔全体をリフトアップ</div>
              </li>
              <li>
                <span>2.エリミネ</span>
                <img src="../images/3/intro_02.jpg" alt="Intro 02">
                <div class="txt">てい鍼とバチ鍼を使いお肌のたるみ・くすみを改善</div>
              </li>
              <li>
                <span>3.ドゥーブルアキュプレッション</span>
                <img src="../images/3/intro_03.jpg" alt="Intro 03">
                <div class="txt">お顔の凝りを確認しながら、指とてい鍼で交互にツボを刺激</div>
              </li>
              <li>
                <span>4.フリクション</span>
                <img src="../images/3/intro_04.jpg" alt="Intro 04">
                <div class="txt">2本のてい鍼を使用。表情筋に対して垂直の刺激を与え、お顔の筋肉を引き上げていく。</div>
              </li>
              <li>
                <span>5.エヴォンタイユ</span>
                <img src="../images/3/intro_05.jpg" alt="Intro 05">
                <div class="txt">指で頬を内側に寄せあげ、てい鍼で口周り、マリオネットライン、ほうれい線をリフトアップ</div>
              </li>
              <li>
                <span>6.トゥルネ</span>
                <img src="../images/3/intro_06.jpg" alt="Intro 06">
                <div class="txt">てい鍼を2本で持ちピーリングをしながらお顔の筋肉を引き上げ、コラーゲンを刺激</div>
              </li>
              <li>
                <span>7.ビブラッション</span>
                <img src="../images/3/intro_07.jpg" alt="Intro 07">
                <div class="txt">指でお肌に振動を与えお顔全体のコラーゲンを刺激。てい鍼でさらに奥を刺激</div>
              </li>
              <li>
                <span>8.リサージュ</span>
                <img src="../images/3/intro_08.jpg" alt="Intro 08">
                <div class="txt">てい鍼を両手で持ち、眼輪筋起始部から外に向かい目元の老化物質や疲れをとる</div>
              </li>
              <li>
                <span>8.エリミネ</span>
                <img src="../images/3/intro_09.jpg" alt="Intro 09">
                <div class="txt">最後にお顔全体に残った老廃物をてい鍼とバチ鍼を用い再度流し、同時にくすみ・ニキビ・シミを改善</div>
              </li>
            </ul>
          </div>
          <!--/.bx_gr_intro-->
          <!--/.bx_serminar_beuty-->
        </div>
        <!--/.gr_intro_beauty-->
        
        <!--/.infor_beauty-->
      </div>
    </div>
    <!--/.bx_bkg-->
    <!-- <a href="#" class="btn btn_cts"><span>受講お申し込みはこちら</span></a> -->
  </div>
  
  <div class="row bkg_common gr_intro_beauty">
    <div class="bx_bkg">
      <div class="inner">
        <div class="bx_serminar_beuty">
          <h3 class="ttl_ser">美容鍼灸修了証発行セミナー、動画、<br class="show_sp">セミナーでの使用道具一覧</h3>
          <div class="bx_ser">
            <figure>
              <img src="../images/3/img_ser.png" alt="Images Serminar">
            </figure>
            <ul class="list_ser">
              <li><em>てい鍼</em><span>2本</span></li>
              <li><em>バチ鍼</em><span>1枚</span></li>
              <li><em>オイル</em><span>1回につき<br>500円玉大の大きさ<br>（5ｍｌくらい）</span></li>
            </ul>
            <!--/.list_ser-->
          </div>
          <!--/.bx_ser-->
        </div>
      </div>
    </div>
  </div>
  
  <div class="st_time">
    <div class="row">
      <div class="bx_bkg_time">
        <div class="inner">
          <h2 class="ttl_time">
            美容鍼灸修了証発行セミナー
            <span>
              締め切り<br><em><strong>7</strong>月<strong>31</strong>日<small>月</small></em>
            </span>
          </h2>
          <!--/.ttl_time-->
          <div class="bx_datetime">
            <div class="left">
              <p class="date">2017年<strong>8</strong>月<strong>6</strong>日<span>日</span></p>
              <p class="time">10:30~16:30<small>（受付10：00開始）</small></p>
            </div>
            <!--/.left-->
            <div class="right">
              <ul class="list_right">
                <li><span>定 員</span><em>先着24名</em></li>
                <li><span>会 場</span>
                  <em>東洋医療専門学校7階実技室
                    <small>東洋医療専門学校7階実技室 会場前</small>
                  </em>
                </li>
                <li><span>アクセス:地下鉄新大阪駅(徒歩5分)/JR新大阪駅(徒歩10分)</span></li>
              </ul>
              <!--/.list_right-->
            </div>
          </div>
          <!--/.bx_datetime-->
        </div>
      </div>
    </div>
  </div>
  
  <div class="dac_extra row">
    <div class="soldout">
      <h3 class="ttl"><img src="images/bg_new_txt.png" alt="おすすめ！"></h3>
      <div class="inner">
        <div class="content">
          <h4><span>5,000円の道具セット付き</span></h4>
          <div class="icon">
            <ul>
              <li>てい鍼<br>2本</li>
              <li>バチ鍼<br>1枚</li>
              <li>オイル<br>20cc <br>2本</li>
            </ul>
          </div>
        </div>
        <div class="price">
          <p><span>受講料</span><em>32,000<small>円</small></em></p>
        </div>
      </div>
    </div><!-- end sold out -->
    <div class="note">※早割り価格での販売は7/10をもちまして終了いたしました。</div>
    <div class="soldout2 soldout">
      <div class="inner">
        <div class="content">
          <h4><span>道具なしの場合</span></h4>
        </div>
        <div class="price">
          <p><span>受講料</span><em>29,000<small>円</small></em></p>
        </div>
      </div>
      
    </div>
    <div class="note">※早割り価格での販売は7/10をもちまして終了いたしました。</div>
  </div>
  
  <div class="row bkg_common">
    <div class="bx_bkg">
      <div class="inner">
        <div class="infor_beauty">
          <h2 class="ttl_intro">セミナー情報</h2>
          <table>
            <tbody>
              <tr>
                <th>鍼灸セラピスト</th>
                <td>鍼灸師/ ハリジェンヌ院長</td>
              </tr>
              <tr>
                <th>講　師 </th>
                <td>光本 朱美 先生</td>
              </tr>
              <tr>
                <th>会　場</th>
                <td>東洋医療専門学校7階実技室</td>
              </tr>
              <tr>
                <th>住　所</th>
                <td>〒532-0004<br>大阪市淀川区西宮原1-5-35</td>
              </tr>
              <tr>
                <th>最寄駅</th>
                <td>地下鉄新大阪駅（徒歩5分）<br>JR新大阪駅（徒歩10分）</td>
              </tr>
              <tr>
                <th>日　程</th>
                <td>2016年00月00日（日）10:30〜16:30</td>
              </tr>
              <tr>
                <th>定　員</th>
                <td>最大:25名</td>
              </tr>
              <tr>
                <th>対　象</th>
                <td>鍼灸の免許を有するもの</td>
              </tr>
              <tr>
                <th>費　用</th>
                <td>30,000円/人（税別）<br><span>受理した受験料は、ノバセルスクール側の<br>試験施行中止などの事情のほかは返還いたしません。</span></td>
              </tr>
              <tr>
                <th>支払い方法</th>
                <td>クレジットカード、またはキャリア決済</td>
              </tr>
              <tr>
                <th>申込方法</th>
                <td>下記「カートに入れる」へお進みください。</td>
              </tr>
              <tr>
                <th>備参加者準物<br>(持参物)</th>
                <td>動画の再生できる端末<br>（スマートフォン、タブレットなど）、白衣</td>
              </tr>
              <tr>
                <th>主　催</th>
                <td>ノバセル（エヌ・アール機器株式会社）</td>
              </tr>
              <tr>
                <th>協　力</th>
                <td><span>東洋医療専門学校 鍼灸師学科 同窓会 滋蓬会 jihoukai</span></td>
              </tr>
              <tr>
                <th>担　当</th>
                <td>登録メールにて本セミナーに関する<br>ご案内を送りいたします。</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>
<!--/.st_introduction-->
