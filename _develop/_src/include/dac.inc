<section id="teacher" class="teacher">
  <div class="logo">
    <div class="row">
      <img src="images/logo.png">
    </div>
  </div>
  <!--/.logo-->
  <div class="ttl">
    <h2 class="row">美容鍼灸修了証発行セミナー</h2>
  </div>
  <div class="wrap row">
    <figure class="bottom">
      <img src="./images/teacher.png" alt="teacher">
    </figure>
    <div class="top">
      <div class="intro">
        <p class="txt-top">ハリジェンヌで実際に施術されている</p>
        <h2 class="ttl_top"><span>刺さない鍼</span>の<br><span>極秘手技</span>を初公開！</h2>
        <p>「動画e-ラーニング」を利用した新しいセミナーのカタチ</p>
      </div>
      <div class="wrap_box">
        <div class="banner">
          <figure><img src="./images/banner.png" alt="banner"></figure>
        </div>
        <div class="box">
          <div class="inner">
            <p class="show_sp"><span>受講料</span> <em>30,000<small>円</small></em></p>
            <a href="" class="btn"><span>受講お申し込みはこちら</span></a>
          </div>
        </div>
        <div class="info">
          <p><img src="./images/hari.png" alt="hari"></p>
          <p><span>講 師</span><em><small>鍼灸師</small>光本 朱美</em></p>
        </div>
      </div>
      <div class="frontof">
        <div class="content_frontof">
          <p>ご好評につき</p>
          <h3>完 売</h3>
          <p>いたしました</p>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- end teacher -->
<div class="st_time">
  <div class="row">
    <div class="bx_bkg_time">
      <div class="inner">
        <h2 class="ttl_time">
          美容鍼灸修了証発行セミナー
          <span>
            締め切り<br><em><strong>7</strong>月<strong>31</strong>日<small>月</small></em>
          </span>
        </h2>
        <!--/.ttl_time-->
        <div class="bx_datetime">
          <div class="left">
            <p class="date">2017年<strong>8</strong>月<strong>6</strong>日<span>日</span></p>
            <p class="time">10:30~16:30<small>（受付10：00開始）</small></p>
          </div>
          <!--/.left-->
          <div class="right">
            <ul class="list_right">
              <li><span>定 員</span><em>先着24名</em></li>
              <li><span>会 場</span>
                <em>東洋医療専門学校7階実技室
                  <small>東洋医療専門学校7階実技室 会場前</small>
                </em>
              </li>
              <li><span>アクセス:地下鉄新大阪駅(徒歩5分)/JR新大阪駅(徒歩10分)</span></li>
            </ul>
            <!--/.list_right-->
          </div>
        </div>
        <!--/.bx_datetime-->
      </div>
    </div>
  </div>
</div>
<!--/.st_time-->
<section class="care row bkg_common">
  <div class="bx_bkg">
    <div class="inner">
      <figure>
        <img src="./images/img_01.png" alt="img">
      </figure>
      <div class="right start">
        <h2>美容鍼灸の技術を習得したい!!<br class="show_sp">こんなこと<br class="show_sp">考えたことありませんか？</h2>
        <ul class="list_check">
          <li>「美容鍼灸を学びたい！<br class="show_sp">でもどこで学べばいいの？」</li>
          <li>「誰のセミナーに行けばいいの？」</li>
          <li>セミナーに行っても曖昧な知識で<br class="show_sp">受講していませんか？</li>
        </ul>
      </div>
      <figure>
        <img src="./images/img_02.png" alt="img">
      </figure>
      <div class="right end">
        <p>ノバセルはそんな方々のために、インターネットで学ぶ講座をご用意いたしました。一見、難しそうに見える美容鍼灸も、動画配信による詳しい解説と高画質の動画で、細部までみることができます。<br>
        また、セミナーでは講師によるデモンストレーションをまじえながら、直接指導を受けることにより、正しい技術をしっかり学べ、美容鍼灸を初心者から本格的に学びたい人まで、確かな知識、技術の習得が可能となります。テクニックを身につければ、きっと、あなたのスキル、治療が変わります。
        「ノバセルスクール美容鍼灸コース」を受講し、修了条件を満たした方にはノバセル(novacell)より「修了証」を発行いたします。</p>
      </div>
    </div>
  </div>
</section>
<!-- end care -->
<section class="amazing row bkg_common">
  <div class="bx_bkg">
    <div class="inner">
      <h2>絶対に身につく美容鍼灸セミナー</h2>
      <div class="wrap">
        <figure>
          <span><img src="./images/img_amazing_01.png" alt="amazing 01"></span>
          <figcaption>自宅で動画学習</figcaption>
        </figure>
        <figure>
          <span><img src="./images/img_amazing_02.png" alt="amazing 02"></span>
          <figcaption>セミナーで先生から直接指導</figcaption>
        </figure>
        <figure>
          <span><img src="./images/img_amazing_03.png" alt="amazing 03"></span>
          <figcaption>修了証発行</figcaption>
        </figure>
      </div>
      <div class="wrap_text">
        <p>さらに、セミナー終了後も一ヶ月間復習可能‼</p>
      </div>
      <div class="bx_st_student">
        <h3 class="ttl_student">だから<br class="show_sp"><span>受講者満足度が高い!!</span></h3>
        <div class="move_youtube"><iframe width="640" height="360" src="https://www.youtube.com/embed/vn3LfpU33tQ?rel=0" frameborder="0" allowfullscreen=""></iframe></div>
        <div class="gr_message">
          <article>
            <div class="top">
              <figure>
                <img src="./images/message_01.png" alt="Images message 01">
              </figure>
              <h3>
                <span><img src="./images/img_message.png" alt="Text Message"></span>
                毎日動画で予習!!<br>セミナーに集中できる
              </h3>
            </div>
            <p>動画は毎日通勤の時に見ていました！<br class="show_pc">
            今までは、セミナーに行ってもその場で<br class="show_pc">
            必死にメモを取っていました。<br class="show_pc">
            先にツボや施術の流れを動画で見ていた<br class="show_pc">
            のでセミナーに集中できてとても解りや<br class="show_pc">
            すかったです！</p>
          </article>
          <article>
            <div class="top">
              <figure>
                <img src="./images/message_02.png" alt="Images message 02">
              </figure>
              <h3>
                <span><img src="./images/img_message.png" alt="Text Message"></span>
                先生から分からない<br>ところを直接指導
              </h3>
            </div>
            <p>動画を見てからセミナーを学べて、わか<br>
            らないところは先生に直接指導頂けたの<br>
            で充実した内容でとても身になりました。<br>
            また、セミナーがあれば参加したいです。</p>
          </article>
        </div>
        <!--/.gr_message-->
      </div>
    </div>
  </div>
</section>
<!-- end amazing -->
<section class="seminar row">
  <h2><ins class="hide_mb"><img class="show_pc" src="./images/span_seminar.png" alt=""> <img class="show_sp" src="./images/banner.png" alt=""></ins><em><span>美容鍼灸修了書発行セミナー</span><br class="show_mb"><span><small>の</small>ポイント</span></em></h2>
  <div class="wrap">
    <article>
      <h3><span>動画配信による学習</span></h3>
      <div class="main_ar">
        <figure><img src="./images/seminar_01_pc.png" alt="seminar" class="resimg" ></figure>
        <div class="txt">自分の好きな時間に美容鍼灸が携帯端末、PC・タブレットで見て学べる‼<br>今までセミナーでしか見ることのできなかった先生の手技。動画は先生目線で何度でも繰り返しご視聴頂けるため、あなたに合った個別学習が可能。</div>
      </div>
    </article>
    <article>
      <h3><span>セミナーによる直接指導</span></h3>
      <div class="main_ar">
        <figure><img src="./images/seminar_02_pc.png" alt="seminar" class="resimg" ></figure>
        <div class="txt">セミナーでは直接指導を受ける事により、正しい技術をしっかり学べます‼<br> 動画で予習を行うことで、基礎知識が理解できるため、普段<br>のセミナーより何倍もいろんなことが吸収可能に！<br>質問があれば、先生が直接ベットを周り、直接指導を行ってくれるので、予習して疑問に思ったこと、わからないことはその場で聞いてより深い知識を習得‼</div>
      </div>
    </article>
    <article>
      <h3><span>修了証発行</span></h3>
      <div class="main_ar">
        <figure><img src="./images/seminar_03_pc.png" alt="seminar" class="resimg" ></figure>
        <div class="txt">修了条件を満たした方には修了証、JGT（ジャパン・ゴールドセラピスト）認定ステッカーが発行されます。</div>
      </div>
    </article>
  </div>
</section>
<!-- end seminar -->
<div class="st_time">
  <div class="row">
    <div class="bx_bkg_time">
      <div class="inner">
        <h2 class="ttl_time">
          美容鍼灸修了証発行セミナー
          <span>
            締め切り<br><em><strong>7</strong>月<strong>31</strong>日<small>月</small></em>
          </span>
        </h2>
        <!--/.ttl_time-->
        <div class="bx_datetime">
          <div class="left">
            <p class="date">2017年<strong>8</strong>月<strong>6</strong>日<span>日</span></p>
            <p class="time">10:30~16:30<small>（受付10：00開始）</small></p>
          </div>
          <!--/.left-->
          <div class="right">
            <ul class="list_right">
              <li><span>定 員</span><em>先着24名</em></li>
              <li><span>会 場</span>
                <em>東洋医療専門学校7階実技室
                  <small>東洋医療専門学校7階実技室 会場前</small>
                </em>
              </li>
              <li><span>アクセス:地下鉄新大阪駅(徒歩5分)/JR新大阪駅(徒歩10分)</span></li>
            </ul>
            <!--/.list_right-->
          </div>
        </div>
        <!--/.bx_datetime-->
      </div>
    </div>
  </div>
</div>
<!--/.st_time-->
<section class="doctor row">
  <h2><em>鍼灸師<span>光本 朱美</span>とは?</em></h2>
  <div class="bkg_common">
    <div class="bx_bkg">
      <div class="inner">
        <article>
          <figure><img src="./images/avatar.png" alt="avatar"></figure>
          <div class="right">
            <h3><span>表参道 ハリジェンヌ 院長</span><em>光本 朱美</em></h3>
            <div class="txt">エステティック等を35年経営する家庭に生まれる。<br class="show_pc">
            14歳の時に「ハリでエステをする」と決意。<br class="show_pc">
            19歳で美容大国フランスのエステティック名門校 キャサリン・サルタン“L’Ecole Catherine Sertin”に 留学し、最年少留学生で資格取得。<br class="show_pc">
            〈3大資格取得〉 ･ 国際資格 世界 CIDESCO エステティック<br class="show_pc">
            ･ 国家資格 フランス CAP エステティック ･ 国家資格 日本 鍼灸師</div>
          </div>
        </article>
        <div class="history">
          <dl>
            <dt><span>2012年</span></dt>
            <dd>
              <p>表参道「ハリジェンヌ」設立</p>
            </dd>
          </dl>
          <dl>
            <dt><span>2014年</span></dt>
            <dd>
              <p>フランス・パリ ヌーヴェルエステティックの 世界美容大会に日本代表として選出。<br>世界初の鍼灸師・最年少エステティシャンで 「頭顔整美」の新技術披露。</p>
            </dd>
          </dl>
          <dl>
            <dt><span>2015年</span></dt>
            <dd><p>フランス研修開催</p></dd>
          </dl>
          <dl>
            <dt><span>2016年</span></dt>
            <dd><p>アメリカカリフォルニア州世界国際エステティック・ スパ大会に日本代表として出場し<br>新技術披露。 アメリカロサンゼルスで美容プロ向け研修開催。</p></dd>
          </dl>
          <dl>
            <dt><span>2017年</span></dt>
            <dd><p>フランス・パリ ヌーヴェルエステティックの 世界美容大会に日本代表として2度目の選出。最新技術を披露。</p></dd>
          </dl>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- end doctor -->