<div class="row">
	<p class="txt_call">何かご不明な点などございましたら<br class="show_sp">ノバセルまでご連絡ください</p>
	<div class="bx_time">
		<a class="tel" href="tel:050-3621-6166"><span>050-3621-6166</span></a>
		<em>受付時間：平日10：00~18：00</em>
	</div>
</div>
<p class="copyright">COPYRIGHT &copy;NOVACELL Inc ALL RIGHTS RESERVED.</p>