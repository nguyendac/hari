<!-- <div class="bx_top_animation">
  <figure>
    <img src="../images/3/king.png" alt="Images King">
  </figure>
  <figure>
    <img src="../images/3/img_ant_db.png" alt="Images Animation">
  </figure>
  <ul class="list_right">
    <li><span>受講期間</span><em>3ヵ月</em></li>
    <li><span>受講料（税抜）</span><em>30,000円 ／ 3ヵ月</em></li>
  </ul>
</div> -->
<section class="st_learn">
  <div class="row bkg_common">
    <div class="bx_bkg">
      <div class="inner">
        <div class="bx_learn_novasel">
          <h2 class="ttl_novasel">ネットで学ぶ<span>ノバセル</span>だからできること</h2>
          <div class="gr_point">
            <article>
              <figure>
                <img src="../images/3/point1new.png" alt="Point 01">
              </figure>
              <h3 class="ttl_art"><span>自宅で</span>予習、復習</h3>
              <p>
                動画はパソコン、スマホ、タブレット
                で視聴可能なため見たいときに、
                いつでも、どこでも、何度でも視聴
                可能！セミナー終了後もノバセルでは
                一ヵ月間、復習期間を設けています。
                確かな技術を習得するためにも家に
                帰ってもう一度練習
              </p>
            </article>
            <article>
              <figure>
                <img src="../images/3/point2new.png" alt="Point 02">
              </figure>
              <h3 class="ttl_art">セミナーで先生から<br class="show_pc"><span>直接指導</span></h3>
              <p>
                事前学習で基礎知識を十分身につ
                けた後は、実際にセミナーで先生か
                ら直接指導！<br>
                事前学習ではわからなかった感覚や、
                より具体的な技術の習得を実感！
              </p>
            </article>
            <article>
              <figure>
                <img src="../images/3/point3new.png" alt="Point 03">
              </figure>
              <h3 class="ttl_art"><span>修了証発行</span></h3>
              <p>
                動画で学び、セミナーにて確かな
                技術を習得、参加された方には
                修了証を発行いたします。
              </p>
              <span class="certificate"><img src="../images/3/certificate.png" alt="Images Certificate"></span>
            </article>
          </div>
          <!--/.gr_point-->
        </div>
        <!--/.bx_learn_novasel-->
        <div class="bx_st_student">
          <h2 class="ttl_student">だから<br class="show_sp"><span>受講者満足度〇〇％！！</span></h2>
          <div class="move_youtube"><iframe width="640" height="360" src="https://www.youtube.com/embed/ifSHM7B6K5w?rel=0" frameborder="0" allowfullscreen></iframe></div>
          <div class="gr_message">
            <article>
              <div class="top">
                <figure>
                  <img src="../images/3/message_01.png" alt="Images message 01">
                </figure>
                <h3>
                  <span><img src="../images/3/img_message.png" alt="Text Message"></span>
                  直接指導を受けられる<br>分かりやすい授業
                </h3>
              </div>
              <p>少人数性なので先生との距離も近いので<br class="show_pc">分からないところは直接教えていただけ<br class="show_pc">ました。</p>
            </article>
            <article>
              <div class="top">
                <figure>
                  <img src="../images/3/message_02.png" alt="Images message 02">
                </figure>
                <h3>
                  <span><img src="../images/3/img_message.png" alt="Text Message"></span>
                  動画をみながら<br>自宅で予習、復習
                </h3>
              </div>
              <p>授業で分からなかった所は授業の動画を<br class="show_pc">確認しながら予習、復習ができるので上達も早いです。</p>
            </article>
          </div>
          <!--/.gr_message-->
        </div>
        <!--/.bx_st_student-->
        <div class="bx_procedure">
          <h2 class="ttl_procedure">【受講の手続きは<span>カンタン</span>】</h2>
          <div class="gr_step">
            <div class="item_st">
              <div class="top_st">
                <div class="stt_step"><em>Step<br><span>1</span></em></div>
                <h3 class="tt_step"><span>【カートに入れる】</span>を<br>クリック</h3>
                <span class="img_vt show_pc">
                  <img src="../images/3/vector_mb.png" alt="Vector Member">
                </span>
              </div>
              <!--/.top_st-->
              <figure>
                <img src="../images/3/img_st_01.png" alt="Images Step 01">
              </figure>
            </div>
            <!--/.item_st-->
            <div class="item_st">
              <div class="top_st">
                <div class="stt_step"><em>Step<br><span>2</span></em></div>
                <h3 class="tt_step"><small>カゴの中の商品をご確認後、<br></small><span>【購入手続きへ】</span>を<br class="show_pc">クリック</h3>
              </div>
              <!--/.top_st-->
              <figure>
                <img src="../images/3/img_st_02.png" alt="Images Step 02">
              </figure>
            </div>
            <!--/.item_st-->
            <div class="item_st">
              <div class="top_st">
                <div class="stt_step"><em>Step<br><span>3</span></em></div>
                <h3 class="tt_step"><small>お支払い方法を選択後</small><br><span>【次へ】</span>をクリック</h3>
                <span class="img_vt show_pc">
                  <img src="../images/3/vector_mb.png" alt="Vector Member">
                </span>
              </div>
              <!--/.top_st-->
              <figure>
                <img src="../images/3/img_st_03.png" alt="Images Step 03">
              </figure>
            </div>
            <!--/.item_st-->
            <div class="item_st">
              <div class="top_st">
                <div class="stt_step"><em>Step<br><span>4</span></em></div>
                <h3 class="tt_step"><small>入力内容をご確認いただき</small><span>【次へ】</span><small>を<br>クリック後</small>、<span>クレジット</span><small>でのお支払いか</small><br><span>キャリア決</span><small>済を選択し、お支払い画面へク</small></h3>
              </div>
              <!--/.top_st-->
              <figure>
                <img src="../images/3/img_st_04.png" alt="Images Step 04">
              </figure>
            </div>
            <!--/.item_st-->
          </div>
          <!--/.gr_step-->
        </div>
        <!--/.bx_procedure-->
      </div>
    </div>
    <!--/.bx_bkg-->
    <a href="#" class="btn btn_cts"><span>受講お申し込みはこちら</span></a>
  </div>
  <!--/.bkg_common-->
</section>